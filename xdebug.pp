package { 'php5-xdebug':
  ensure => present,
}

# NB: You need to adjust remote_host to be your IP...

php5::config {
  'xdebug.remote_enable': value => 1;
  'xdebug.remote_port': value => 9000;
  'xdebug.profiler_enable': value => 0;
  'xdebug.profiler_enable_trigger': value => 1;
  'xdebug.profiler_output_dir': value => "/vagrant/tmp";
  'xdebug.trace_output_dir': value => "/vagrant/tmp";
  'xdebug.trace_enable_trigger': value => 1;
  'xdebug.trace_format': value => 1;
  'xdebug.remote_connect_back': value => 'On';
  'html_errors': value => 'On';
}

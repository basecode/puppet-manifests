package { 'sqlite3':
  ensure => present
}

package { 'libsqlite3-dev':
  ensure => present
}

package { 'mailcatcher':
  provider => gem,
  ensure => present,
  require => Package['sqlite3', 'libsqlite3-dev']
}

php5::config {
  'sendmail_path': value => '"catchmail -f developer-vm@bigcommerceapp.com"',
}

# @todo figure out how to do this automagically
#
# To start mailcatcher: `vagrant ssh` and run `mailcatcher --ip=0.0.0.0`
# You can then browse <domain>:1080 for the web interface
# Send emails from PHP as normal
#
# Once installed you need to adjust the store config as follows:
#
# MailUseSMTP: 1
# MailSMTPServer: localhost
# MailSMTPPort: 1025

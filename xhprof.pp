#
# Note requires: mod 'zeleznypa/xhgui' & mod 'puppetlabs/vcsrepo'
#

package { ['php-pear', 'php5-dev', 'build-essential', 'graphviz']:
  ensure => present,
}

exec { 'xhprof-install':
  command => '/usr/bin/pecl install pecl.php.net/xhprof-0.9.4',
  creates => '/usr/share/php/xhprof_html',
  require => [Package['build-essential'], Package['php-pear'], Package['php5-dev']],
}

file { '/etc/php5/conf.d/xhprof.ini':
  content  => 'extension=xhprof.so',
  require => Exec['xhprof-install'],
  notify => Exec['reload-nginx'],
}

# Database schema
file {
    "xhprof db schema":
      ensure => file,
      path   => "/tmp/xhprof.sql",
      content => "CREATE TABLE `details` (
  `id` char(17) NOT NULL,
  `url` varchar(255) default NULL,
  `c_url` varchar(255) default NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `server name` varchar(64) default NULL,
  `perfdata` MEDIUMBLOB,
  `type` tinyint(4) default NULL,
  `cookie` BLOB,
  `post` BLOB,
  `get` BLOB,
  `pmu` int(11) unsigned default NULL,
  `wt` int(11) unsigned default NULL,
  `cpu` int(11) unsigned default NULL,
  `server_id` char(3) NOT NULL default 't11',
  `aggregateCalls_include` varchar(255) DEFAULT NULL,
  PRIMARY KEY  (`id`),
  KEY `url` (`url`),
  KEY `c_url` (`c_url`),
  KEY `cpu` (`cpu`),
  KEY `wt` (`wt`),
  KEY `pmu` (`pmu`),
  KEY `timestamp` (`timestamp`)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;"
}->
mysql::db { 'xhprof':
  user      => 'xhprof',
  password  => 'xhprof',
  host      => 'localhost',
  grant     => [all],
  sql       => '/tmp/xhprof.sql',
  require  => File["xhprof db schema"]
}->
exec { 'xhprof-schema':
  command => '/usr/bin/mysql -uroot -pmagic xhprof < /tmp/xhprof.sql',
  require => [MySQL::DB['xhprof']],
}

# Clone XHGui for looking at the collected data.
vcsrepo { "/var/www/vhosts/xhprof/":
    ensure => present,
    provider => git,
    source => "https://github.com/preinheimer/xhprof.git"
}

# @todo Setup the config file.

# Setup the nginx vhost xhprof.bcapp.dev
nginx::site{'xhprof':
  content => 'server {
  listen 80;
  server_name xhprof.bcapp.dev;

  root /var/www/vhosts/xhprof/xhprof_html/;
  index index.php;

  location / {
      try_files $uri $uri/ /index.php;
  }

  location ~ ^.+\.php {
    try_files $uri =404;
    include /etc/nginx/fastcgi_params;
    fastcgi_pass "unix:/var/run/php5-fpm/bigcommerce_app.sock";
  }
}',
  notify => Exec['reload-nginx']
}

# Adjust Nginx PHP config to include the profiling code & restart.
file { '/etc/nginx/conf.d/xhprof.conf':
  content  => 'fastcgi_param  PHP_VALUE   "auto_prepend_file=/var/www/vhosts/xhprof/external/header.php \n auto_append_file=/var/www/vhosts/xhprof/external/footer.php";',
  require => Exec['xhprof-install'],
  notify => Exec['reload-nginx'],
}